import API from '../Config/Config';
import axios from 'axios';
export default {
    login(request){ 
        return axios.post(`http://localhost:8090/api/login`, request)
    },
    checkToken(request){
        return API.post(`check-token`, request)
    }
}