import API from '../Config/Config';
export default {
    getList(request){
        return API.post(`BarangList/a`, request)
    },
    saveBarang(request){
        return API.post(`saveBarang`, request)
    },
    getNamaBarang(request){
        return API.post(`getNamaBarang`, request)
    }
}