import React, { Component } from 'react';

import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Login from './components/Login'
import Logout from './components/Logout'
import Home from './components/Home';
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login} ></Route>
          <Route path="/home" component={Home} ></Route>
          <Route path="/logout" component={Logout} ></Route>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;