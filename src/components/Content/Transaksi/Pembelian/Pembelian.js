import React, { Component } from 'react'
import ListPembelian from './ListPembelian'
import { Table, Input, Button, Space, Tag } from 'antd';
import 'antd/dist/antd.css';
import ModalPembelian from './ModalPembelian'
import TransaksiPembelianService from '../../../../Services/TransaksiPembelianService'
class Pembelian extends Component{
    constructor(props){
        super(props)
        const token = localStorage.getItem("token")
        let loggedIn = true
        if(token == null)
        {
            loggedIn = false
        }
        this.state = {
            loggedIn ,
            data : []
        }
    }
    getList = () =>
    {
        TransaksiPembelianService.getList({

        })
        .then((resp)=>{
            let data = resp.data.responseData;
            console.log('data',data)
            let dataResp = [];
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                dataResp.push({
                    transaksiId : element.transaksiId,
                    transaksiNama : element.transaksiNama,
                    tanggal : element.tanggal,
                    hargaTotal : element.hargaTotal,
                    detail : element.detail_transaksi_pembelian,
                    status : (element.status == 1 ? <Tag color="green" key={1}>
                    Active
                  </Tag> : <Tag color="volcano" key={0}>
                    In Active
                  </Tag>),
                })
            }
            this.setState({data : dataResp})
        })
    }

    componentDidMount()
    {
        this.getList();
    }

    render(){
        return(
            <div>
                <ModalPembelian judul="New Transaksi Pembelian" />
                <br></br>
                <ListPembelian data = {this.state.data} reInit = {this.getList} />
            </div>
        )
    }
}
export default Pembelian;