import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';

import { Table, Input, Button, Space, Tag,Badge, Menu, Dropdown } from 'antd';
import Highlighter from 'react-highlight-words';
import { SearchOutlined,DownOutlined } from '@ant-design/icons';
import ModalPembelian from './ModalPembelian'


const ListPembelian = (props) => {

    const columns = [
        {
          title: 'Nama Transaksi',
          dataIndex: 'transaksiNama',
          key: 'transaksiNama',
        },
        {
            title: 'Harga Total',
            dataIndex: 'hargaTotal',
            key: 'hargaTotal',
        },
        {
          title: 'Tanggal',
          dataIndex: 'tanggal',
          key: 'tanggal',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title : 'Action',
            dataIndex : 'action',
            key : 'action',
            render: (value,row,index) => 
              <span>
                <ModalPembelian judul = "Edit" reInit = {props.reInit} dataRecord = {row}   />
              </span>
        }
      ];
    return <Table className="components-table-demo-nested" columns={columns} dataSource={props.data} />;
}

export default ListPembelian;