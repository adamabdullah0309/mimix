import React, { useState,Component } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Button, Modal, Tabs, Input, Radio,Form, Tag,message, DatePicker, Row, Col, Divider } from 'antd';
import moment from 'moment';
import TableModal from './TableModal'
const CollectionCreateForm = ({ visible, onCreate, onCancel,dataRecord,coba }) => {
  const { TabPane } = Tabs;
    
    const [form] = Form.useForm();
      const data = dataRecord;
      let barangObject = {}
      const style = { background: '#0092ff', padding: '8px 0' };
      const style2 = {  padding: '8px 0' };
      console.log('data',data)
      if(data !== undefined){
        barangObject = {
            transaksiId : data.transaksiId == undefined ? null : data.transaksiId,
            transaksiNama : data.transaksiNama == undefined ? null : data.transaksiNama,
            hargaTotal : data.hargaTotal == undefined ? null : data.hargaTotal,
            tanggal : data.tanggal === undefined ? null :  moment(data.tanggal, "YYYY-MM-DD"),
            status :  data.status === undefined ? parseInt(1,10) : parseInt(data.status.key, 10)
        }
      }
      form.setFieldsValue(barangObject)
      return (
        // { data === undefined ? null : data}
        // {console.log('as')}
        
        <Modal
          width = {1300}
          visible={visible}
          title="New barang"
          okText="Submit"
          cancelText="Cancel"
          onCancel={onCancel}
          onOk={() => {
            form
              .validateFields()
              .then(values => {
                // console.log('form',form)
                onCreate(values);
                // coba(values)
                // form.resetFields();/
              })
              .catch(info => {
                console.log('Validate Failed:', info);
              });
          }}
        >
          <Form
            form={form}
            layout="vertical"
            name="form_in_modal"
            initialValues={{
              modifier: 'public',
            }}
            
          >
            <Row>
            <Col span={6}>
          
            
            <Form.Item
              name="transaksiNama"
              label="Nama Transaksi"
              rules={[
                {
                  required: true,
                  message: 'Please input nama transaksi !'
                }
              ]}
            >
              <Input  />
              
            </Form.Item>
            <Form.Item name="hargaTotal" label="Harga Total">
              <Input disabled />
            </Form.Item>
            <Form.Item name="tanggal" label="Tanggal">
              <DatePicker />
            </Form.Item>
  
            <Form.Item name="status" label="Status" className="collection-create-form_last-form-item"  initialValue = {1} >
              <Radio.Group >
                  <Radio value={1}>Active</Radio>
                  <Radio value={0}>Inactive</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
          <Col span={1} ></Col>
          <Col span = {17}>
          <Tabs defaultActiveKey="1">
            <TabPane tab="Barang" key="1">
            <TableModal dataRecord={data} formProps = {form} onCreate = {onCreate} coba = {coba} />
            </TabPane>
            <TabPane tab="Inventory" key="2">
              Content of Tab Pane 2
            </TabPane>
          </Tabs>
              
          </Col>
          </Row>
          </Form>
        </Modal>
      );
    };
class ModalPembelian extends Component {
    // const [visible, setVisible] = useState(false);
    constructor(props){
      super(props)
      this.state = {
        visible : false,
        detail : [],
        header : this.props.dataRecord
      }
    }

    
    onCreate = (values) => {
        let finalData = values;
        // let hargaTotal = 0;
        finalData.tanggal =  moment(finalData.tanggal).format("YYYY-MM-DD");
        finalData.detail_transaksi_pembelian = this.state.detail;
        // finalData.detail_transaksi_pembelian.map((data,index)=>{
         
        // })
        console.log('finalData',finalData)
    }
    onClick = () =>{
        this.setState({
          visible: true
        });
    }
    
    coba = (data)=>{
      
      let dataDetail = data;
      let hargaTotal = 0

      dataDetail.map((data,index)=>{
        if(data.hargaSatuan !== undefined || data.quantity !== undefined){
        hargaTotal = hargaTotal + ( ( data.hargaSatuan * data.quantity )  )
        }

        if(data.diskon !== undefined){
          hargaTotal = hargaTotal - data.diskon
          }
      })
      // this.state.header.hargaTotal = 2
      // console.log('this.props.dataRecord',this.props.dataRecord)
      // if(this.props.dataRecord !== undefined)
      // {
      //   this.props.dataRecord.hargaTotal = hargaTotal
      // }
      console.log('this.state.header',this.state.header)
      let data1 = {
        hargaTotal : hargaTotal,
        transaksiId : this.state.header.transaksiId,
        transaksiNama : this.state.header.transaksiNama,
        tanggal :   moment(this.state.header.tanggal, "YYYY-MM-DD"),
        status :  (this.state.header.status == 1 ? <Tag color="green" key={1}>
                  Active
                </Tag> : <Tag color="volcano" key={0}>
                  In Active
                </Tag>)
      }
      this.setState({ header : data1})
      console.log('hargaTotal',hargaTotal)
      this.setState({
        detail : data
      })
    }

    render()
    {
      return (
        <div>
          <Button
            type="primary"
            onClick={this.onClick}
          >
            {this.props.judul}
          </Button>
          <CollectionCreateForm
            visible={this.state.visible}
            onCreate={this.onCreate}
            coba = {this.coba}
            onCancel={() => {
              this.setState({
                visible: false
              });
            }}
            dataRecord = {this.state.header}
          />
        </div>
      );
    }
    
}

export default ModalPembelian;