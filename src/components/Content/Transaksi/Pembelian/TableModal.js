import React, { useContext, useState, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './TableModal.css'
import NamaBarang from '../../../../Services/BarangService'
import { Table, Input, Button, Popconfirm, Form, AutoComplete, Select } from 'antd';

import { UserOutlined } from '@ant-design/icons';
const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};



const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  wajib,
  handleSave,
  changeBarang,
  namaBarang,
  ...restProps
}) => {
  const { Option } = Select;
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const descriptionRef = useRef(null);
  const form = useContext(EditableContext);



  form.setFieldsValue({
    [dataIndex]: record == undefined ? "" : record[dataIndex],
  });
  useEffect(() => {
    form.validateFields()
  });

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async e => {
    try {
     
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  const barangHandler = (event) =>{
    // try {
    //   const values = await form.validateFields();
    //   toggleEdit();
      changeBarang({...record,event});
    // } catch (errInfo) {
    //   console.log('Save failed:', errInfo);
    // }
  }

  let childNode = children;
  let wajibTrue = null
  if(wajib == true){
    wajibTrue =
        [{
          required: true,
          message : `${title.props.children[0]} is required.`,
        }]
      
  }

  if (typeof children[1] !=='object') {
    if(dataIndex === "barangId"){
      childNode = (
        <div>
        <Form.Item
          style={{
            margin: 0,
          }}
          name="barangId"
          rules={
              wajibTrue
          }
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Pilih barang"
            optionFilterProp="children"
            // onBlur={barangHandler}
            // onPressEnter={barangHandler} 
            // filterOption={(input, option) =>
            //   option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            // }
            onChange={(event) => barangHandler(event)}
          >
            {namaBarang.map((data,index)=>{
              return(
              <Option  value={data.barangId}>{data.barangNama}</Option>)
            })}
          </Select>
        </Form.Item>
        </div>
      ) 
    }
    else if(dataIndex == "satuan") {
      childNode = (
        <Form.Item
          style={{
            margin: 0,
          }}
          name="satuan"
          rules={
              wajibTrue
          }
        >
          <Input disabled ref={inputRef} onPressEnter={save} onChange={save}  onBlur={save} />
        </Form.Item>
      )
     }
   else{
    childNode = (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={
            wajibTrue
        }
      >
        <Input ref={inputRef} onPressEnter={save} onChange={save} onBlur={save} />
      </Form.Item>
    )
   }
    
  }
  
  return <td {...restProps}>{childNode}</td>;
};

class TableModal extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: <div>Nama Barang <span style={{ color: "#FF0000"  }}>*</span></div>,
        dataIndex: 'barangId',
        width: '10%',
        editable: true,
        wajib : true
      },
      {
        title : 'Description',
        dataIndex: 'description',
        width: '15%',
        editable: true,
        wajib : false,
      },
      {
        title: 'Diskon',
        dataIndex: 'diskon',
        editable :true,
        wajib : false
      },
      {
        title: 'Satuan',
        dataIndex: 'satuan',        
        wajib : false,
        editable:true

      },
      {
        title:  <div>Harga Satuan <span style={{ color: "#FF0000"  }}>*</span></div>,
        dataIndex: 'hargaSatuan',
        editable :true,
        wajib : true
      },
      {
        title:  <div>Quantity <span style={{ color: "#FF0000"  }}>*</span></div>,
        dataIndex: 'quantity',
        editable : true,
        wajib : true
      },
      {
        title: 'Action',
        dataIndex: 'Action',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.key)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      },
    ];
    this.state = {
        dataSource : props.dataRecord == undefined ? [] : props.dataRecord.detail ,
        count: props.dataRecord == undefined  ? 0 : props.dataRecord.detail.length,
        namaBarang : []
    };
    // this.props.coba(this.state.dataSource)
  }

  componentDidMount(){
    this.getNamaBarang()
  }

  handleDelete = key => {
    const dataSource = [...this.state.dataSource];
    this.setState({
      dataSource: dataSource.filter(item => item.key !== key),
    });
  };

  getNamaBarang = () =>{
    NamaBarang.getNamaBarang({
  
    })
    .then((resp)=>{
        let data = resp.data.responseData;
        let dataResp = [];
        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            dataResp.push({
              barangNama : element.barangNama,
              barangId : element.barangId,
              satuan : element.satuan
            })
        }
        this.setState({namaBarang : dataResp})
    })
  }

  handleAdd = () => {
    const { count, dataSource } = this.state;
    const newData = {
      key: count
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  };

  changeBarang = (event) =>{
    console.log('event',event)
    
    // console.log('values',values)
    const newData = [...this.state.dataSource];

    const barang = this.state.namaBarang;
    const barangIndex = barang.findIndex(item => event.event === item.barangId)
    // console.log('barangIndex',barangIndex)
    
    const barangSatuan = barang[barangIndex].satuan;
    event.satuan = barangSatuan
    event.barangId = barang[barangIndex].barangId
    event.barangNama = barang[barangIndex].barangNama
    const index = newData.findIndex(item => event.key === item.key);
    const item = newData[index];
    
    newData.splice(index, 1, { ...item, ...event });
    this.setState({
      dataSource: newData,
    });
    
    this.props.coba(newData)
  }

  handleSave = row => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData,
    });
    this.props.coba(newData)
  };

  

  render() {
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          wajib : col.wajib,
          namaBarang : this.state.namaBarang,
          handleSave: this.handleSave,
          changeBarang : this.changeBarang
        }),
      };
    });
    return (
      <div>
        <Button
          onClick={this.handleAdd}
          type="primary"
          style={{
            marginBottom: 16,
          }}
        >
          Add a row
        </Button>
        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
      </div>
    );
  }
}

export default TableModal;