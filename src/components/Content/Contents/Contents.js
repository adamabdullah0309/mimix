import React, { Component } from 'react'
import { Layout, Menu } from 'antd';
import 'antd/dist/antd.css';
import Barang from '../Master/Barang/Barang';
import Inventory from '../Master/Inventory/Inventory';
import Pembelian from '../Transaksi/Pembelian/Pembelian';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;
const contents = () =>{
    return (
        <Content style={{ padding: '0 24px', minHeight: 280 }}>
                <Switch>
                    <Route path="/home/barang" component={Barang}/>
                    <Route path="/home/inventory" component={Inventory}/>
                    <Route path="/home/pembelian" component={Pembelian}/>
                </Switch>
        </Content>
    )
}

export default contents
