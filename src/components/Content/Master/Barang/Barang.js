import React, { Component,Redirect } from 'react'
import ModalBarang from './ModalBarang'
import { Table, Input, Button, Space, Tag } from 'antd';
import 'antd/dist/antd.css';
import ListBarang from './ListBarang'
import BarangService from '../../../../Services/BarangService'
class Barang extends Component {
    constructor(props){
        super(props)
        const token = localStorage.getItem("token")
        let loggedIn = true
        if(token == null)
        {
            loggedIn = false
        }
        this.state = {
            loggedIn ,
            data : []
        }
    }

    getList = () =>
    {
        BarangService.getList({

        })
        .then((resp)=>
        {
            
            let data = resp.data.responseData;
            let dataResp = [];
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                dataResp.push({
                    barangId : element.barangId,
                    barangNama : element.barangNama,
                    description : element.description,
                    quantity : element.quantity,
                    satuan : element.satuan,
                    status : (element.status == 1 ? <Tag color="green" key={1}>
                    Active
                  </Tag> : <Tag color="volcano" key={0}>
                    In Active
                  </Tag>),
                })
            }
            this.setState({data : dataResp})
            console.log('Barang',this.state.data)
        })
    }

    componentDidMount()
    {
        this.getList();
    }

    shouldComponentUpdate(nextProps,nextState){
        return true;
    }

    render() {

        console.log("halooo")
        if(this.state.loggedIn === false)
        {
            return <Redirect to="/" />
        }
        return (
            <div>
                <ModalBarang judul = "New Barang" reInit = {this.getList} />
                <br></br>
                <ListBarang data = {this.state.data} reInit = {this.getList} />
            </div>
        )
    }
}

export default Barang;
