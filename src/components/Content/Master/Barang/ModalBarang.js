import React, { Component, useState } from 'react'
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Button, Modal,  Input, Radio,Form, message } from 'antd';
import BarangService from '../../../../Services/BarangService'


// const { SubMenu } = Radio;


const CollectionCreateForm = ({ visible, onCreate, onCancel,dataRecord }) => {
  const [form] = Form.useForm();
    const data = dataRecord;
    let barangObject = {}

    if(data !== undefined){
      barangObject = {
        barangId : data.barangId,
        barangNama: data.barangNama,
        description : data.description,
        satuan : data.satuan,
        status : parseInt(data.status.key, 10)
      }
    }
    form.setFieldsValue(barangObject)
    return (
      // { data === undefined ? null : data}
      // {console.log('as')}
      <Modal
        visible={visible}
        title="New barang"
        okText="Submit"
        cancelText="Cancel"
        onCancel={onCancel}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              // console.log('form',form)
              onCreate(values);
              // form.resetFields();/
            })
            .catch(info => {
              console.log('Validate Failed:', info);
            });
        }}
      >
        <Form
          form={form}
          layout="vertical"
          name="form_in_modal"
          initialValues={{
            modifier: 'public',
          }}
          
        >
          <Form.Item
            name="barangId"
            label="Id Barang"
            hidden = {true}
          >
            <Input />
          </Form.Item>
          
          <Form.Item
            name="barangNama"
            label="Nama Barang"
            rules={[
              {
                required: true,
                message: 'Please input nama barang !'
              }
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="satuan"
            label="Satuan"
            rules={[
              {
                required: true,
                message: 'Please input Satuan !'
              }
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item name="description" label="Description">
            <Input type="textarea" />
          </Form.Item>

          <Form.Item name="status" label="Status" className="collection-create-form_last-form-item"  initialValue = {data === undefined ? 1 : parseInt(data.status.key, 10)} >
            <Radio.Group >
                <Radio value={1}>Active</Radio>
                <Radio value={0}>Inactive</Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      </Modal>
    );
};
  
  const ModalBarang = (props) => {
    const [visible, setVisible] = useState(false);
    
    const onCreate = (values) => {
      console.log('Received values of form: ', values);
      setVisible(false);
      let barangObject = {
        barangNama: values.barangNama,
        description : values.description == null ? "" : values.description,
        satuan : values.satuan,
        status : values.status
      }
      if(values.barangId !== null){
        barangObject.barangId = values.barangId;
      }
      
      BarangService.saveBarang(barangObject).
      then((resp) =>{
        props.reInit();
      })
      .catch((err)=>{
        message.error({content : 'error', duration : 2})
      })
    };
    const onClick = () =>{
      setVisible(true);
      console.log('props.datarecord = ', props.dataRecord)
    }
    return (
      <div>
        <Button
          type="primary"
          onClick={onClick}
        >
          {props.judul}
        </Button>
        <CollectionCreateForm
          visible={visible}
          onCreate={onCreate}
          onCancel={() => {
            setVisible(false);
          }}
          dataRecord = {props.dataRecord}
        />
      </div>
    );
  };

  export default ModalBarang;