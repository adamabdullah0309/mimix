import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import BarangService from '../../../../Services/BarangService'
import { Table, Input, Button, Space, Tag } from 'antd';
import Highlighter from 'react-highlight-words';
import { SearchOutlined } from '@ant-design/icons';
import ModalBarang from './ModalBarang'

class ListBarang extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            data : []
        }
    }

  render() {
    const columns = [
        {
          title: 'Barang Nama',
          dataIndex: 'barangNama',
          key: 'barangNama',
        },
        {
          title: 'Description',
          dataIndex: 'description',
          key: 'description',
        },
        {
            title : 'Quantity',
            dataIndex : 'quantity',
            key : 'quantity'
        },
        {
          title: 'Satuan',
          dataIndex: 'satuan',
          key: 'description',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Action',
            key: 'action',
            render: (value,row,index) => 
              this.props.data.length >= 1 ? (
              <span>
                <ModalBarang judul = "Edit" reInit = {this.props.reInit} dataRecord = {row} />
              </span>
              ) : null,
          }
      ];
    return <Table columns={columns} dataSource={this.props.data} />;
  }
}

export default ListBarang;