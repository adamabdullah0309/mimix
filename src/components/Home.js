import React, { Component } from 'react'
import { Link , Redirect,HashRouter} from 'react-router-dom'
import 'antd/dist/antd.css';
import { Layout, Menu } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import HeaderPage from './Header/HeaderPage';
import SideMenu from './SideMenu/SideMenu';
import Breadcrumb from './Breadcrumb/Breadcrumb'
import Contents from './Content/Contents/Contents'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import LoginService from '../Services/LoginService'
import Session from '../Session/Session'
const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;


export default class Admin extends Component {
    constructor(props){
        super(props)
        const token = localStorage.getItem("token")
        let loggedIn = true
        if(token == null)
        {
            loggedIn = false
        }
        this.state = {
            loggedIn,
            namaMenu : null
        }
    }

    componentDidMount(){
        this.checkToken()
    }

    checkToken = () => {
        LoginService.checkToken({
            token : 'Bearer ' + Session.getSession('token')
        })
        .catch((error) =>{
            window.location.href = '/';
        })
    }

    onClickHanlder = ( menuName ) =>{
        // this.setState({namaMenu : menuName});
        console.log('menuName = ',menuName)
    }

    render() {
        let contents = (
            <Content style={{ padding: '0 24px', minHeight: 280 }}>
                <div>Welcome</div>
            </Content>
        )
        return (
            <Layout>
                <HeaderPage/>
                
                <Content style={{ padding: '0 50px' }}>
                    <Breadcrumb />
                    <Layout className="site-layout-background" style={{ padding: '24px 0' }}>
                        <SideMenu click={this.onClickHanlder} />
                        <Contents />
                    </Layout>
                </Content>

                <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
        )
    }
}
