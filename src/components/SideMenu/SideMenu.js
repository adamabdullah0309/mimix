import React, { Component } from 'react'
import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined, PieChartOutlined } from '@ant-design/icons';

import { BrowserRouter, Switch, Route,Link,Redirect } from 'react-router-dom';
const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;
export default class SideMenu extends Component {
    constructor(props)
    {
        super(props)
    }
    render() {
        return (
            <Sider className="site-layout-background" width={200}>
                <Menu
                mode="inline"
                defaultOpenKeys={['sub1']}
                style={{ height: '100%' }}
                >
                    <SubMenu key="sub1" icon={<UserOutlined />} title="Master">
                        <Menu.Item key="inventory" onClick={this.props.click}>
                            <Link to="/home/inventory"  style={{ textDecoration: 'none' }}>
                                Inventory
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="barang">
                            <Link to="/home/barang"  style={{ textDecoration: 'none' }}>
                                Barang
                            </Link>
                        </Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub2" icon={<LaptopOutlined />} title="Transaksi">
                        <Menu.Item key="pembelian">
                            <Link to="/home/pembelian"  style={{ textDecoration: 'none' }}>
                                Pembelian
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="6">Penjualan</Menu.Item>
                    </SubMenu>
                    <Menu.Item key="1" icon={<PieChartOutlined />}>
                        Laporan Keuangan
                    </Menu.Item>
                </Menu>
            </Sider>
        )
    }
}
