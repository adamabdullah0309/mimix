import React, { Component } from 'react'
import { HashRouter as Router, Route, Switch, Link, withRouter } from 'react-router-dom';
import { Layout, Menu, Breadcrumb  } from 'antd';
import 'antd/dist/antd.css';

const breadcrumbNameMap = {
    '/home' : 'Home',
    '/home/barang': 'Barang',
    '/home/inventory': 'Inventory',
    '/home/pembelian' : 'Pembelian'
};

const Home = withRouter(props => {
    const { location } = props;
    const pathSnippets = location.pathname.split('/').filter(i => i);
    console.log('pathsno',pathSnippets)
    const extraBreadcrumbItems = pathSnippets.map((_, index) => {
        
      const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
      console.log('url',url)
      return (
        <Breadcrumb.Item key={url}>
          <Link to={url}>{breadcrumbNameMap[url]}</Link>
        </Breadcrumb.Item>
      );
    });
    const breadcrumbItems = [
      <Breadcrumb.Item style={{ margin: '16px 0' }}>
      </Breadcrumb.Item>,
    ].concat(extraBreadcrumbItems);
    return (
        <Breadcrumb style={{ margin: '16px 0' }}>{breadcrumbItems}</Breadcrumb>
    );
  });
export default Home;