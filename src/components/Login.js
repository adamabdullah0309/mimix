import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'
import LoginService from '../Services/LoginService'
import Session from '../Session/Session'
import { Layout, Menu,message } from 'antd';
const key = 'updatable';

class Login extends Component {
    constructor(props){
        super(props);
        // const token = localStorage.getItem("token")

        let loggedIn = true
        // if(token == null)
        // {
        //     loggedIn = false
        // }
        this.state = {
            username : "",
            password : "" ,
            loggedIn,
            type : ""
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    componentDidMount(){
        this.checkToken()
    }

    checkToken = () => {
        LoginService.checkToken({
            token : 'Bearer ' + Session.getSession('token')
        })
        .then((resp) =>{
            window.location.href = '/home';
        })
        .catch((error) =>{
            message.error({ content: 'Please Login Again', key, duration: 2 });
        })
    }

    submitFrom = (e) => {
        e.preventDefault()
        const { username, password } =this.state
        
        LoginService.login({
            username : username,
            password : password
        })
        .then((resp) => 
        {
            let data = resp.data
            console.log(data)
            if (data.status === 200) {
                Session.setSession(data.result)
                message.success({ content: data.message, key, duration: 2 });
                this.setState({ isLogged : true })
            }
            window.location.href = '/home';
        }).catch((resp) =>
        {
            message.error({ content: 'Please check your username and password!', key, duration: 2 });
        })

        // if( username ==="A"  && password === "B")
        // {
        //     this.setState({loggedIn : true})
        // }
        // else if( username ==="A1"  && password === "B1")
        // {
        //     this.setState({loggedIn : true})
        // }
    }

    render() {
        // if(this.state.loggedIn)
        // {
        //     return <Redirect to="/dashboard"></Redirect>
        // }
        return (
            <div>
                <h1>Login</h1>
                <form onSubmit={this.submitFrom} >
                    <input type="text" placeholder="username" name="username" value={this.state.username} onChange={this.onChange}></input>
                    <br></br>
                    <input type="text" placeholder="password" name="password" value={this.state.password} onChange={this.onChange}></input>
                    <br></br>
                    <input type="submit"></input>
                </form>
            </div>
        )
    }
}

export default Login