import React, { Component } from 'react'
import { Link,Redirect } from 'react-router-dom'
import { Menu, Dropdown, message, Button  } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import LogoutCss from './Logout.module.css'
import Session from '../Session/Session';
export default class Logout extends Component {
    constructor(props){
        super(props)
    }

    onClick = () => {
        Session.clearSession();
        window.location.href = '/';
    };
      

    confirm = () =>{
        message.success('See yaa !');
    }

    cancel = () =>{
        message.error('Fiuhh');
    }

    render() {
        const menu = (
            <Menu onClick={this.onClick}>
                <Menu.Item key="1">Logout</Menu.Item>
            </Menu>
        );
        return (
            <div style={{float: 'right'}}>
                <Dropdown overlay={menu} >
                    <a  onClick={this.onClick}>
                        <DownOutlined className={LogoutCss.blue} style={{fontSize: '24px'}}  />
                    </a>
                </Dropdown>
            </div>
        )
    }
}
