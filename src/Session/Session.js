export default {
	setSession(data) {
		// localStorage.setItem('appsId', data.responseData.AppsId);
		// localStorage.setItem('accId', data.responseData.accId);
		// localStorage.setItem('accName', data.responseData.accName);
		// localStorage.setItem('appsName', data.responseData.appsName);
		// localStorage.setItem('companyId', data.responseData.companyId);
		// localStorage.setItem('companyName', data.responseData.companyName);
		// localStorage.setItem('divisionId', data.responseData.divisionId);
		// localStorage.setItem('divisionName', data.responseData.divisionName);
		// localStorage.setItem('fullname', data.responseData.fullname);
		// localStorage.setItem('titleId', data.responseData.titleId);
		// localStorage.setItem('titleName', data.responseData.titleName);
		localStorage.setItem('username', data.username);
		localStorage.setItem('token', data.token);
	},
	clearSession() {
		localStorage.clear();
	},
	// setSessionByKey(key, value) {
	// 	localStorage.setItem(key, value);
	// },
	getSession(key) {
		return localStorage.getItem(key);
	}
	// TokenIsExpired(request) {
	// 	return axios.post(`http://18.136.105.155:8090/taskmanagement-0.0.1-SNAPSHOT/api/check-token`, request)
	// },
	// clearCacheServer(request) {
	// 	return axios.post(`http://18.136.105.155:8090/taskmanagement-0.0.1-SNAPSHOT/api/clear-cache`, request)
	// }
}
